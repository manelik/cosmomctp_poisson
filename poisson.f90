



program poisson

!
! Resuelve la ecuación de Poisson en simetría esférica
!
!  __2         n
!  \/ u = rho*u
!


  implicit none

  integer i,j,k,iter   !counters

  integer Nr

  real(8) dr
  
  integer ghost   !ghostpoints

  integer maxiter ! Máximo número de iteraciones

  integer bound   ! condicion de frontera

  integer n_nonlinear

  real(8) epsilon, res !Tolerancia
  
  real(8), allocatable, dimension (:) :: r,u,C0,C1,C2,rho !Coefficients of the tridiagonal matrix

  real(8), allocatable, dimension (:) :: auxarray

  real(8) a_0,r_0,s_0  ! Gaussian parameters

  real(8) aux

  character(30) order

  print*, ""
  print*, "Numero de puntos Nr"
  read(*,*) Nr

  print*, ""
  print*, "Separacion dr"
  read(*,*) dr

!  print*, ""
!  print*, "Ghostpoints ghost"
!  read(*,*) ghost

  print*, ""
  print*, "Coeficiente del termino no lineal rho*u^n"
  read(*,*) n_nonlinear

  print*, ""
  print*, "Amplitud del dato inicial a_0"
  read(*,*) a_0

  print*, ""
  print*, "Posicion del dato inicial r_0"
  read(*,*) r_0

  print*, ""
  print*, "Ancho del dato inicial s_0"
  read(*,*) s_0

  print*, ""
  print*, "bound =  0:  Robin (u0 is the value at infinity)"
  print*, "bound = +1:  Dirichlet (u0 is the boundary value)"
  print*, "bound = -1:  Newmann (u0 is the derivative at the boundary)"
  read(*,*) bound


! Declaro aquí el número de puntos auxiliares para asignar el tamaño
! correcto de los arreglos

! Esto es lo que no funcionaba en el ejemplo en clase
  ghost = 3

! Allocate arrays

  allocate(u(1-ghost:Nr),C0(1-ghost:Nr),C1(1-ghost:Nr),C2(1-ghost:Nr),&
       &  rho(1-ghost:Nr),r(1-ghost:Nr))

  allocate(auxarray(1-ghost:nr))

!  deallocate(auxarray)

! Initialize

  do i = 1-ghost, Nr
     r(i) = (dble(i)-0.5d0)*dr
     auxarray(i) = 0.d0
  end do

  rho = a_0* (exp(-(r-r_0)**2/s_0**2) + exp(-(r+r_0)**2/s_0**2))

  if (n_nonlinear == 0) then
! Independent source

! Fill matrix coefficients

     C0 = 2.d0/r
     C1 = 0.d0
     C2 = rho

! Call matrix inversion.
     order =  "two"
     call invertmatrix(ghost,Nr,dr,1.d0,u,C0,C1,C2,+1,order,0)
                                                 ! symm +1 -1
  
  elseif(n_nonlinear == 1) then
! Linear source
! Fill matrix coefficients

     C0 = 2.d0/r
     C1 = -rho
     C2 = 0.d0

! Call matrix inversion.
     order =  "two"
  
     call invertmatrix(ghost,Nr,dr,1.d0,u,C0,C1,C2,+1,order,0)
                                                 ! symm +1 -1
  

  else
!  elseif(n_nonlinear > 1) then
! Non-linear source
! In this case we give a trial function and  iterate

!    Set up initial guess for conformal factor.
     u = 1.d0

!    Initialize residual.
     res = 1.d0
     epsilon = 1.d-10
     maxiter = 1000

!    Begin iterations.
     iter = 0

     

     do while ((res.gt.epsilon).and.(iter.lt.maxiter))

        iter = iter + 1

!       Save old conformal factor.

        auxarray = u

!       Fill in coefficients of linearized equation.

        C0 = 2.d0/r
        C1 = 0.d0
        C2 = -rho*auxarray**n_nonlinear

!       Call matrix inversion.

        call invertmatrix(ghost,Nr,dr,1.d0,u,C0,C1,C2,+1,order,0)
                                                 ! symm +1 -1

!       Find residual.

        res = 0.d0

        do i=1,Nr
           aux = abs(u(i)-auxarray(i))
           if (aux>res) res = aux
        end do

       print *,iter,res

     end do

!    Message in case we reached the maximum iteration number.

     if (iter>=maxiter) then
        print *,'Maximum iteration number reached in idata_scalarpulse.f90.'
        print *
     end if


  end if
! Open outfiles

  open(unit=88,file="poisson.dat",status="unknown")

  do i = 1, Nr
     write(88,"(3ES16.8)") r(i), u(i), rho(i)
  end do

  close(88)


end program poisson



subroutine invertmatrix(ghost,Nr,dr,u0,u,A,B,C,sym,order,bound)

! **********************************************
! ***   SOLVE LINEAR POISSON-TYPE EQUATION   ***
! **********************************************

! This subroutine inverts a band-diagonal matrix that
! solves a linear Poisson-type equation of the form:
!
!  2
! d u  +  A(r) d u  + B(r) u  =  C(r)
!  r            r
!
! Notice that we assume that the coefficient of the second
! derivative has already been normalized to 1.
!
! The outer boundary condition depends on the value of the
! integer parameter "bound":
!
!       bound =  0:  Robin (u0 is the value at infinity)
!       bound = +1:  Dirichlet (u0 is the boundary value)
!       bound = -1:  Newmann (u0 is the derivative at the boundary)
!
! For the solutions we use the routines  "bandec" and "banbks"
! from NUMERICAL RECIPES.  They can be found at the end of this
! file, with very minor changes to suit the situation here.
!
! They are used one after the other to invert the band-diagonal
! matrix.

  implicit none

  integer i,Nr,NP
  integer sym,ghost,bound

  real(8) dr,u0
  real(8) aux

  real(8), dimension (1-ghost:Nr) :: u,A,B,C

  character(len=*) order

  integer, dimension (0:Nr) :: indx

  real(8), dimension (0:Nr) :: S
  real(8), dimension (0:Nr,-2:2) :: M,ML


! ************************
! ***   SANITY CHECK   ***
! ************************

  if (abs(sym)/=1) then
     print *
     print *, 'invertmatrix: Symmetry parameter must be +-1'
     print *
     stop
  end if

  if (abs(bound)>1) then
     print *
     print *, 'invertmatrix: Boundary parameter must be (0,+-1)'
     print *
     stop
  end if


! **********************
! ***   INITIALIZE   ***
! **********************

! Initialize to zero.

  M  = 0.d0
  ML = 0.d0
  S  = 0.d0


! ***********************
! ***   SOURCE TERM   ***
! ***********************

! Remember that we multiply everything with dr**2,
! so the source term for the matrix inversion is
! actually dr**2 times the source of the differential
! equation.

  do i=1,Nr-1
     S(i) = dr**2*C(i)
  end do


! ******************
! ***   ORIGIN   ***
! ******************

! At left boundary we impose symmetry conditions.

  M(0,0) = 1.d0
  M(0,1) = -sym


! ************************
! ***   SECOND ORDER   ***
! ************************

  if (order=="two") then

!    Interior points.

     do i=1,Nr-1
        M(i,-1) = 1.d0 - 0.5d0*A(i)*dr
        M(i, 0) = - 2.d0 + B(i)*dr**2
        M(i,+1) = 1.d0 + 0.5d0*A(i)*dr
     end do


! ************************
! ***   FOURTH ORDER   ***
! ************************

  else

!    Point i=1.  Here we use the symmetries to avoid making
!    reference to the point at i=-1.

     i = 1

     M(i,-1) = + 16.d0 - 8.d0*A(i)*dr
     M(i, 0) = - 30.d0 + 12.d0*B(i)*dr**2
     M(i,+1) = + 15.d0 + 9.d0*A(i)*dr
     M(i,+2) = - 1.d0 - A(i)*dr

     S(i) = 12.d0*S(i)

!    Interior points.

     do i=2,Nr-2

        M(i,-2) = - 1.d0 + A(i)*dr
        M(i,-1) = + 16.d0 - 8.d0*A(i)*dr
        M(i, 0) = - 30.d0 + 12.d0*B(i)*dr**2
        M(i,+1) = + 16.d0 + 8.d0*A(i)*dr
        M(i,+2) = - 1.d0 - A(i)*dr

        S(i) = 12.d0*S(i)

     end do

!    Point i=Nr-1. This is only second order at the moment!

     i = Nr-1

     M(i,-1) = 1.d0 - 0.5d0*A(i)*dr
     M(i, 0) = - 2.d0 + B(i)*dr**2
     M(i,+1) = 1.d0 + 0.5d0*A(i)*dr

  end if


! ******************************
! ***   BOUNDARY CONDITION   ***
! ******************************

  if (bound==0) then

! Robin boundary condition:
!
!    d u  =  (u0 - u) / r
!     r
!
! This is only second order at the moment!

     aux = 0.5d0/real(Nr-1)

     M(Nr,-1) = - 1.d0 + aux
     M(Nr, 0) = + 1.d0 + aux

     S(Nr) = 2.d0*aux*u0

  else if (bound==+1) then

! Dirichlet boundary condition:
!
!    u = u0

     M(Nr,0) = 1.d0

     S(Nr) = u0

  else if (bound==-1) then

! Newmann boundary condition:
!
!    d u  =  u0
!     r
!
! This is only second order at the moment!

     M(Nr,-1) = - 1.d0
     M(Nr, 0) = + 1.d0

     S(Nr) = u0*dr

  end if


! ***********************
! ***   CALL SOLVER   ***
! ***********************

! Call solver. On output the solution is
! contained in S.

  NP = Nr+1
  call bandec(M,NP,2,2,ML,indx)
  call banbks(M,NP,2,2,ML,indx,S)

! Copy solution.

  do i=0,Nr
     u(i) = S(i)
  end do


! ************************
! ***   GHOST POINTS   ***
! ************************

! Ghost points using symmetries.

  do i=1,ghost
     u(1-i) = sym*u(i)
  end do


! ***************
! ***   END   ***
! ***************

end subroutine invertmatrix



subroutine bandec(a,n,m1,m2,al,indx)

! This routine performs an LU decomposition of a
! band diagonal matrix.
!
! On input, a(n,m1+m2+1) contains the matrix elements
! defining the system.  Here "n" is the number of
! equations, and (m1,m2) indicate how many subdiagonals
! and superdiagonals are different from zero (e.g. for
! a tridiagonal matrix m1=m2=1, and for a pentadiagonal
! (m1=m2=2).
!
! On output, "a" now contains the upper triangular matrix
! and "al" the lower diagonal.  The integer vector "indx"
! records the row permutation effected by the partial pivoting
! and is required by subroutine "banbks".

  implicit none

  integer n,m1,m2
  integer i,j,k,l,mm

  integer indx(n)

  real(8) a(n,m1+m2+1),al(n,m1+m2+1)
  real(8) d,dum,TINY

  parameter (TINY=1.e-20)

  mm=m1+m2+1
  l=m1

  do i=1,m1
     do j=m1+2-i,mm
        a(i,j-l)=a(i,j)
     end do
     l=l-1
     do j=mm-l,mm
        a(i,j)=0.d0
     end do
  end do
 
  d=1.d0
  l=m1

  do k=1,n

    dum=a(k,1)
    i=k
 
    if (l.lt.n) l=l+1

    do j=k+1,l
       if (abs(a(j,1)).gt.abs(dum)) then
          dum=a(j,1)
          i=j
       end if
    end do

    indx(k)=i

    if (dum.eq.0.) a(k,1)=TINY

    if (i.ne.k) then
       d=-d
       do j=1,mm
          dum=a(k,j)
          a(k,j)=a(i,j)
          a(i,j)=dum
       end do
    end if

    do i=k+1,l
       dum=a(i,1)/a(k,1)
       al(k,i-k)=dum
       do j=2,mm
          a(i,j-1)=a(i,j)-dum*a(k,j)
       end do
       a(i,mm)=0.d0
    end do
 
  end do

end subroutine bandec





subroutine banbks(a,n,m1,m2,al,indx,b)

! This routine solves a band-diagonal system of equations.
! The subroutine must be called immediately after "bandec".
!
! On input, "a" contains the upper triangular matrix,
! and "al" the lower triangular.  Here "n" is the number of
! equations, and (m1,m2) indicate how many subdiagonals
! and superdiagonals are different from zero (e.g. for
! a tridiagonal matrix m1=m2=1, and for a pentadiagonal
! (m1=m2=2).
!
! Also on input "b" contains the right hand side source vector,
! and "indx" records the row permutation effected by the partial
! pivoting as obtained in "bandec".
!
! On output, the vector "b" contains the solution.

  implicit none

  integer n,m1,m2
  integer i,k,l,mm
  
  integer indx(n)

  real(8) b(n),a(n,m1+m2+1),al(n,m1+m2+1)
  real(8) dum  

  mm=m1+m2+1
  l=m1

  do k=1,n
     i=indx(k)
     if (i.ne.k) then
        dum=b(k)
        b(k)=b(i)
        b(i)=dum 
     end if
     if (l.lt.n) l=l+1
     do i=k+1,l
        b(i)=b(i)-al(k,i-k)*b(k)
     end do
  end do

  l=1

  do i=n,1,-1
     dum=b(i)
     do k=2,l
        dum=dum-a(i,k)*b(k+i-1)
     end do
     b(i)=dum/a(i,1)
     if (l.lt.mm) l=l+1
  end do

end subroutine banbks

